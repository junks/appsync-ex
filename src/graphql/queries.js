/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getSetting = `query GetSetting($id: ID!) {
  getSetting(id: $id) {
    id
    bio
    include_location
    date_fmt
    owner
  }
}
`;
export const listSettings = `query ListSettings(
  $filter: ModelSettingFilterInput
  $limit: Int
  $nextToken: String
) {
  listSettings(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      bio
      include_location
      date_fmt
      owner
    }
    nextToken
  }
}
`;
