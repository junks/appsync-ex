/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createSetting = `mutation CreateSetting($input: CreateSettingInput!) {
  createSetting(input: $input) {
    id
    bio
    include_location
    date_fmt
    owner
  }
}
`;
export const updateSetting = `mutation UpdateSetting($input: UpdateSettingInput!) {
  updateSetting(input: $input) {
    id
    bio
    include_location
    date_fmt
    owner
  }
}
`;
export const deleteSetting = `mutation DeleteSetting($input: DeleteSettingInput!) {
  deleteSetting(input: $input) {
    id
    bio
    include_location
    date_fmt
    owner
  }
}
`;
