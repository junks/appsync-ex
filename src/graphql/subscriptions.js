/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateSetting = `subscription OnCreateSetting($owner: String!) {
  onCreateSetting(owner: $owner) {
    id
    bio
    include_location
    date_fmt
    owner
  }
}
`;
export const onUpdateSetting = `subscription OnUpdateSetting($owner: String!) {
  onUpdateSetting(owner: $owner) {
    id
    bio
    include_location
    date_fmt
    owner
  }
}
`;
export const onDeleteSetting = `subscription OnDeleteSetting($owner: String!) {
  onDeleteSetting(owner: $owner) {
    id
    bio
    include_location
    date_fmt
    owner
  }
}
`;
